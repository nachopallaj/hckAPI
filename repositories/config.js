const sqlite3 = require('sqlite3').verbose();
// Databases
var usersDb = require('./users.rep');
var db;
function connect() {
  db = new sqlite3.Database("./db/hack.db", sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, (err) => {
    if (err) {
      console.error(err.message);
      throw err;
    }
    usersDb.connect(db);
  });
}

module.exports = {
  connect
}