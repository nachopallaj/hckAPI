var Web3 = require('web3');
web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
abi = JSON.parse('[{"constant":false,"inputs":[{"name":"mes","type":"bytes32"}],"name":"totalPesoPara","outputs":[{"name":"","type":"uint8"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"mes","type":"bytes32"}],"name":"validness","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"bytes32"}],"name":"pesosRecibidos","outputs":[{"name":"","type":"uint8"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"x","type":"bytes32"}],"name":"bytes32ToString","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"listaMeses","outputs":[{"name":"","type":"bytes32"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"mes","type":"bytes32"},{"name":"peso","type":"uint8"}],"name":"pesoParaMes","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"obtenerDescuento","outputs":[{"name":"","type":"uint8"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"ase","type":"string"}],"name":"obtenerDescuentAse","outputs":[{"name":"","type":"uint8"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"contractOwner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"inputs":[{"name":"nombreMeses","type":"bytes32[]"}],"payable":false,"type":"constructor"}]')
VotingContract = web3.eth.contract(abi);
// In your nodejs console, execute contractInstance.address to get the address at which the contract is deployed and change the line below to use your deployed address
contractInstance = VotingContract.at('0x9efa5cf5d42f7e3a393eb21c50773f6524e3bf52');
candidates = {"Enero": "candidate-1", "Febrero": "candidate-2", "Marzo": "candidate-3"}
const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre'];
const aseguradoras = ['Sanitas',  'Adeslas', 'Mapfre'];

function insertarMes( nombreMes, peso ) {
  return new Promise((resolve, reject) => {
    contractInstance.pesoParaMes(nombreMes, peso, {from: web3.eth.accounts[0]}, function() {
        console.log('peso insertado');
        resolve('OK');
      });
    });
}

function obtenerPesoMes( nombreMes ) {
  return contractInstance.totalPesoPara.call(nombreMes).toString();
}

function obtenerPesoMeses( ) {
    let jsonMeses = [];
    meses.forEach(mes => {
      let newMes = {};
      newMes.y = contractInstance.totalPesoPara.call(mes).toString();
      newMes.label = mes;
        jsonMeses.push(newMes);
    });
    return jsonMeses;
}

function obtenerDescuentos() {
  let jsonDescuentos = [];
  aseguradoras.forEach(aseg => {
    let newDescuento = {};
    newDescuento.aseguradora = aseg;
    newDescuento.descuento = contractInstance.obtenerDescuentAse.call(aseg,{from: web3.eth.accounts[0]}).toString();
    jsonDescuentos.push(newDescuento);
  });
  return jsonDescuentos;
}

module.exports = {
  insertarMes,
  obtenerPesoMes,
  obtenerPesoMeses,
  obtenerDescuentos
}