var express = require('express');
var router = express.Router();
var blockchainService = require('../services/blockchain.service');
const actions = [
    'operWeight',
    'OperQueryOffer',
    'OperQueryWeight',
];
const meses = {
    1: 'Enero',
    2: 'Febrero',
    3: 'Marzo',
    4: 'Abril',
    5: 'Mayo',
    6: 'Junio',
    7: 'Julio',
    8: 'Agosto',
    9: 'Septiembre',
    10: 'Octubre',
    11: 'Noviembre',
    12: 'Diciembre'
}

router.post('/', function (req, res, next) {
    const params = req.body.queryResult.parameters;
    const intent = req.body.queryResult.intent.displayName;
    console.log('ACCION DEL BOT: ' + intent);
    console.log(params);
    switch(intent) {
        case 'operWeight':
            operWeight(params).then(response => res.json({fulfillmentText: 'Peso insertado correctamente'}));
            break;
        case 'OperQueryWeight':
            res.json({fulfillmentText: operQueryWeight(params)});
            break;
        case 'OperQueryOffer':
            res.json({fulfillmentText: operQueryOffer()});
            break;
        default:
            res.json({fulfillmentText: 'Error. No se entiende la accion'});
            break;
    }
});

function operWeight(params){
    const peso = params.weight;
    return blockchainService.insertarMes('Noviembre', peso);
}

function operQueryOffer(params) {
    let response = 'Las siguientes aseguradoras te ofrecen descuentos en tus pólizas.'
    blockchainService.obtenerDescuentos().forEach(
        descuento => {
            response += `${descuento.aseguradora} te ofrece un ${descuento.descuento} por ciento de descuento.`
        }
    );
    return response;
}

function operQueryWeight(params) {
    let nombreMes = meses[params.month];
    let pesoMes = blockchainService.obtenerPesoMes(nombreMes);
    return `En ${nombreMes} tu peso era de ${pesoMes} kilos`;
}

module.exports = router;