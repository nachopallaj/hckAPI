var express = require('express');
var router = express.Router();
var blockchainService = require('../services/blockchain.service');

router.post('/', function (req, res, next) {
    const peso = req.query.peso;
    const mes = req.query.mes;
    console.log('Peso: ' + peso );
    console.log('mes: ' + mes );
    blockchainService.insertarMes( mes, peso ).then(response=>res.json({OK:'OK'}));
});

router.get('/', function (req, res, next) {
    res.json(blockchainService.obtenerPesoMeses());
});


router.get('/descuentos', function (req, res, next) {
    res.json(blockchainService.obtenerDescuentos());
});

router.get('/:nombreMes', function (req, res, next) {
    res.json(blockchainService.obtenerPesoMes(req.params.nombreMes));
});

module.exports = router;