var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


// Routers
var loginRouter = require('./routes/login');
var indexRouter = require('./routes/index');
var botRouter = require('./routes/bot');
var blockchainRouter = require('./routes/blockchain');
// Middleware
var jwtAuth = require('./middleware/auth.token');
var cors = require('cors');
// Databases
var dbConfig = require('./repositories/config');

dbConfig.connect();
var app = express();
const context = '/hck/api'

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//Protected view jwt
app.use( context + '/protected', jwtAuth.protected, indexRouter);
app.use( context + '/login', loginRouter);
app.use( context + '/bot', botRouter);
app.use( context + '/blockchain', blockchainRouter );

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
// error handler
app.use(function(err, req, res, next) {
  res.status( err.status || 500);
  res.json( {error: err.message || 'Unknown error' });
});

module.exports = app;
