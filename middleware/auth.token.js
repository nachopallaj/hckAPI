var jwt = require('jsonwebtoken');
var createError = require('http-errors');
const hoursToExpire = 3;
const TOKEN_SECRET = 'hckTON';

function createToken(user_id) {
  return jwt.sign({
    id: user_id,
  }, TOKEN_SECRET, {
    expiresIn: 60 * 60 * hoursToExpire
  });
};

function protected(req, res, next) {
  var token = req.headers['x-access-token'];
  if (!token) return next({status: 401, message: 'Unauthorized'});
  jwt.verify(token, TOKEN_SECRET, function (err, decoded) {
    if (err) return next({status: 401, message: 'Unauthorized'});
    req.verified = true;
    req.token = decoded;
    next();
  });
}

module.exports = {
  createToken,
  protected
};
