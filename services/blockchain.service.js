const blockchainRep = require('../repositories/blockchain.rep');

function insertarMes( nombreMes, peso ) {
    console.log('insertar peso mes: ' + nombreMes + peso );
    return blockchainRep.insertarMes(nombreMes, peso);
}
  
function obtenerPesoMes( nombreMes ) {
    return blockchainRep.obtenerPesoMes(nombreMes);
}

function obtenerPesoMeses() {
    return blockchainRep.obtenerPesoMeses();
}

function obtenerDescuentos() {
    return blockchainRep.obtenerDescuentos();
}

module.exports = {
    insertarMes,
    obtenerPesoMes,
    obtenerPesoMeses,
    obtenerDescuentos
}